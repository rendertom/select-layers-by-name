![Select Layers by Name](/Select%20Layers%20by%20Name.png)

# Select Layers by Name #
Select Layers by Name is a docable After Effects script to select layers by name in the timeline.

### Installation: ###
Clone or download this repository and copy **Select Layers by Name.jsx** to ScriptUI Panels folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking Window -> **Select Layers by Name**

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------