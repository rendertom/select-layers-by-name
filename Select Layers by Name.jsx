/**********************************************************************************************
    Select layers by Name.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Selects layers that match givent string.
		Shift+click appends to selection.
		Alt+click removes from selection.
		Ctrl+click selects everything except layers with a given name.
**********************************************************************************************/

(function (thisObj) {

	scriptBuildUI(thisObj);

	function scriptBuildUI(thisObj) {
		var win = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Select Layers", undefined, {
			resizeable: true
		});

		win.orientation = "row";
		win.spacing = 2;

		win.textField = win.add("edittext", undefined, "");
		win.textField.alignment = ["fill", "top"];
		win.textField.preferredSize.width = 150;
		win.textField.preferredSize.height = 24;

		win.button = win.add("button", undefined, "Go!");
		win.button.alignment = ["right", "top"];
		win.button.preferredSize.height = 24;
		win.button.helpTip = "Select layers with given a name\nShift+click will append to selection\nAlt+click will remove from selection\nCtrl+click will select everything except layers with a given name.";

		win.button.onClick = function () {
			if (win.textField.text === "")
				return;

			var myComp = app.project.activeItem;
			if (!myComp || !(myComp instanceof CompItem))
				return alert("Please select composition first");

			var layerName = "^" + win.textField.text;
			var keyboardState = ScriptUI.environment.keyboardState;
			if (keyboardState.shiftKey) {
				selectLayers(myComp, layerName);
			} else if (keyboardState.altKey) {
				removeFromSelection(myComp, layerName);
			} else if (keyboardState.ctrlKey || keyboardState.metaKey) {
				selectEverythingExcept(myComp, layerName);
			} else {
				deselectLayers(myComp);
				selectLayers(myComp, layerName);
			}
		};

		win.onResizing = win.onResize = function () {
			this.layout.resize();
		};

		win instanceof Window ? win.show() : win.layout.layout(true);
	}

	function selectLayers(comp, layerName) {
		for (var i = 1, il = comp.numLayers; i <= il; i++)
			comp.layer(i).name.match(layerName) && comp.layer(i).selected = true;
	}

	function deselectLayers(comp) {
		for (var i = comp.selectedLayers.length - 1; i >= 0; i--)
			comp.selectedLayers[i].selected = false;
	}

	function removeFromSelection(comp, layerName) {
		for (var i = 1, il = comp.numLayers; i <= il; i++)
			comp.layer(i).name.match(layerName) && comp.layer(i).selected = false;
	}

	function selectEverythingExcept(comp, layerName) {
		for (var i = 1, il = comp.numLayers; i <= il; i++)
			comp.layer(i).selected = comp.layer(i).name.match(layerName) ? false : true;
	}
})(this);